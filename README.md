# Befunge 93

100% naive C# implementation of Befunge 93

## Implementation Notes

* Division by zero just results in 0.
  * Befunge93 does not specify what should happen. The reference implementation prompts the user if running interactively,
otherwise it just uses the numerator.
* Negative modulus is C-like (1, -1) result on test program.

## Tests

The tests are not perfectly named for what they are testing, some handle multiple things.

Many of them were pulled from the [Official? Spec](https://github.com/catseye/Befunge-93)

The test suite should have 100% coverage of the Befunge93 class; but is probably missing some of the ternary branches.
