using NUnit.Framework;

namespace Befunge93.Tests;

public class OperatorTests
{
    private Befunge93 _befunge93;

    [OneTimeSetUp]
    public void OneTimeSetup()
    {
        _befunge93 = new Befunge93();
    }

    [TestCase("12+34+@", new[] { 3, 7 }, TestName = "AddOperator")]
    [TestCase("12-21-@", new[] { -1, 1 }, TestName = "SubtractOperator")]
    [TestCase("23*56*@", new[] { 6, 30 }, TestName = "MultiplyOperator")]
    [TestCase("84/48/@", new[] { 2, 0 }, TestName = "DivideOperator")]
    [TestCase("80/@", new[] { 0 }, TestName = "DivisionByZeroOperator")]
    [TestCase("32%@", new[] { 1 }, TestName = "ModuloOperator")]
    [TestCase("0!1!22+!22-!@", new[] { 1, 0, 0, 1 }, TestName = "LogicalNegationOperator")]
    [TestCase("12#3@", new[] { 1, 2 }, TestName = "BridgeOperator")]
    public void BasicOperator(string program, int[] expectedStack)
    {
        _befunge93.Load(new[] { program });
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var expected = new Stack<int>(expectedStack);
        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expected));
        actual.PrintToTest();
    }

    [TestCase("12345@", new[] { 1, 2, 3, 4, 5 }, TestName = "PushOperator")]
    [TestCase("12345$$$$@", new[] { 1 }, TestName = "PopOperator")]
    [TestCase("1:2:@", new[] { 1, 1, 2, 2 }, TestName = "DuplicateOperator")]
    [TestCase("12\\@", new[] { 2, 1 }, TestName = "SwapOperator")]
    public void StackOperations(string program, int[] expectedStack)
    {
        _befunge93.Load(new[] { program });
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var expected = new Stack<int>(expectedStack);
        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expected));
        actual.PrintToTest();
    }

    [Test]
    public void AllDirections()
    {
        // >1v
        // @ 2
        // 4  
        // ^3<
        var program = new[] { ">1v", "@ 2", "4  ", "^3<" };

        _befunge93.Load(program);
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var expected = new Stack<int>(new[] { 1, 2, 3, 4 });
        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expected));
        actual.PrintToTest();
    }

    [Test]
    public void RandomDirection()
    {
        // > 1 v
        //   @ 2
        //   3  
        // @3? <
        //   3
        //   @
        var program = new[] { "> 1 v", "  @ 2", "  3  ", "@3? <", "  3  ", "  @  ", };

        _befunge93.Load(program);
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var expected = new Stack<int>(new[] { 1, 2, 3 });
        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expected));
        actual.PrintToTest();
    }

    // v@
    // 12
    // >|
    //  3
    //  @
    [TestCase(new[] { "v@", "12", ">|", " 3", " @", }, new[] { 2 }, TestName = "VerticalIfUp")]
    public void VerticalIf(string[] program, int[] expectedStack)
    {
        _befunge93.Load(program);
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expectedStack));
        actual.PrintToTest();
    }

    [TestCase("65`.@", "1 ", TestName = "IsGreaterThan")]
    [TestCase("25`.@", "0 ", TestName = "IsNotGreaterThan")]
    public void GreaterThan(string program, string expectedOutput)
    {
        _befunge93.Load(new[] { program });

        using var sw = new StringWriter();
        Console.SetOut(sw);

        _befunge93.Run(TimeSpan.FromSeconds(1));
        Assert.That(sw.ToString(), Is.EqualTo(expectedOutput));

        TestContext.Out.WriteLine($"Result: '{sw}'");
    }

    // Wrapping Right
    // go right, skip end, go down, go right, push 1, wrap, push 2, go up, end
    //
    // #@v
    // 2^_1
    [TestCase(new[] { "#@v", "2^_1", }, new[] { 1, 2 }, TestName = "HorizontalWrappingRight")]
    // Wrapping Left
    // go right, skip end, push 1, go down, pop 1, go left, push 2, wrap, push 1, end 
    //
    // #@1v
    //   2_@1
    [TestCase(new[] { "#@1v", "  2_@1", }, new[] { 2, 1 }, TestName = "HorizontalWrappingLeft")]
    public void HorizontalWrapping(string[] program, int[] expectedStack)
    {
        _befunge93.Load(program);
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(new Stack<int>(expectedStack)));
        actual.PrintToTest();
    }

    // start by going down; skip over end; nothing on stack, proceed down;
    // push 1; push 2; wrap; skip end; something on stack, go up; end
    //
    // v
    // #
    // @
    // |
    // 1
    // 2
    [TestCase(new[] { "v", "#", "@", "|", "1", "2" }, new[] { 1 }, TestName = "VerticalWrappingDown")]
    // start right; go down; push 1; go left; should go up (skipping @); wrap; push 2
    //
    //  v
    // @1
    // #
    // |<
    // @
    // 2
    [TestCase(new[] { " v", "@1", "#", "|<", "@", "2" }, new[] { 2 }, TestName = "VerticalWrappingUp")]
    public void VerticalWrapping(string[] program, int[] expectedStack)
    {
        _befunge93.Load(program);
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expectedStack));
        actual.PrintToTest();
    }

    [Test]
    public void TestModuloSign()
    {
        // From: https://github.com/catseye/Befunge-93/blob/master/eg/testmodu.bf
        //
        // The original implementation of Befunge-93 was in ANSI C (a.k.a C89).
        // The description of Befunge-93 did not describe how modulo should be
        // implemented for a negative modulus -- it relied on ANSI C's semantics.
        // 
        // Unfortunately, ANSI C did not define negative modulus either.
        // 
        // So this program tests what your Befunge-93 implementation does for
        // modulo by negative numbers.  If it outputs:
        // 
        //  1 -1 : result has same sign as the dividend (like C99)
        // -2  2 : result has same sign as the divisor  (like Python)
        // 
        // Of course, since it is undefined, other results are possible.
        //
        _befunge93.Load(new[] { "703-%.07-3%.@" });

        using var sw = new StringWriter();
        Console.SetOut(sw);

        _befunge93.Run(TimeSpan.FromSeconds(1));
        Assert.That(sw.ToString(), Is.EqualTo("1 -1 "));

        TestContext.Out.WriteLine($"Result: '{sw}'");
    }

    [Test]
    public void TestGettingData()
    {
        // https://esolangs.org/wiki/Befunge#Quine
        _befunge93.Load(new[]
        {
            "01->1# +# :# 0# g# ,# :# 5# 8# *# 4# +# -# _@",
        });

        using var swStdout = new StringWriter();
        Console.SetOut(swStdout);

        _befunge93.Run(TimeSpan.FromSeconds(1));
        Assert.That(swStdout.ToString(), Is.EqualTo("01->1# +# :# 0# g# ,# :# 5# 8# *# 4# +# -# _@"));

        TestContext.Out.WriteLine($"Result: '{swStdout}'");
    }

    [Test]
    public void TestPuttingData()
    {
        // This puts 64 at 1,1 (ascii '@'); this should stop execution
        // (otherwise it would wrap, push 2, then stop)
        _befunge93.Load(new[]
        {
            "79*1+11pv",
            "  1     <@2"
        });
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var expected = new Stack<int>(new[] { 1 });
        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expected));
        actual.PrintToTest();
    }

    [Test]
    public void Timeout()
    {
        var program = new[] { "><" };
        _befunge93.Load(program);
        Assert.Throws<TimeoutException>(
            () => _befunge93.Run(TimeSpan.FromSeconds(0.025)));
    }

    [Test]
    public void InvalidInstruction()
    {
        var program = new[] { "asdf" };
        _befunge93.Load(program);
        Assert.Throws<InvalidInstructionException>(
            () => _befunge93.Run(TimeSpan.FromSeconds(0.025)));
    }
}
