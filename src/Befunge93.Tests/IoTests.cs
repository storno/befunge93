using NUnit.Framework;

namespace Befunge93.Tests;

public class IoTests
{
    private Befunge93 _befunge93;

    [OneTimeSetUp]
    public void OneTimeSetup()
    {
        _befunge93 = new Befunge93();
    }

    [Test]
    public void AsciiOutput()
    {
        var program = new[] { "665+*1-.@" };
        _befunge93.Load(program);

        using var sw = new StringWriter();
        Console.SetOut(sw);

        _befunge93.Run(TimeSpan.FromSeconds(1));
        Assert.That(sw.ToString(), Is.EqualTo("65 "));

        TestContext.Out.WriteLine($"Result: '{sw}'");
    }

    [Test]
    public void InvalidAsciiOutput()
    {
        var program = new[] { "999**,@" };
        _befunge93.Load(program);

        using var sw = new StringWriter();
        Console.SetOut(sw);

        Assert.Throws<InvalidOperationException>(
            () => _befunge93.Run(TimeSpan.FromSeconds(1)));
    }

    [Test]
    public void NumericOutput()
    {
        var program = new[] { "665+*1-,@" };
        _befunge93.Load(program);

        using var sw = new StringWriter();
        Console.SetOut(sw);

        _befunge93.Run(TimeSpan.FromSeconds(1));
        Assert.That(sw.ToString(), Is.EqualTo("A"));

        TestContext.Out.WriteLine($"Result: '{sw}'");
    }

    [Test]
    public void AsciiInput()
    {
        var program = new[] { "~.@" };
        _befunge93.Load(program);

        using var swStdout = new StringWriter();
        Console.SetOut(swStdout);
        using var swStdIn = new StringReader("A");
        Console.SetIn(swStdIn);

        _befunge93.Run(TimeSpan.FromSeconds(1));
        Assert.That(swStdout.ToString(), Is.EqualTo("65 "));

        TestContext.Out.WriteLine($"Result: '{swStdout}'");
    }

    [Test]
    public void InvalidAsciiInput()
    {
        var program = new[] { "~.@" };
        _befunge93.Load(program);

        using var swStdout = new StringWriter();
        Console.SetOut(swStdout);
        using var swStdIn = new StringReader("\ud83d\ude3c");
        Console.SetIn(swStdIn);

        Assert.Throws<InvalidOperationException>(
            () => _befunge93.Run(TimeSpan.FromSeconds(1)));
    }

    [Test]
    public void NumericInput()
    {
        var program = new[] { "&,@" };
        _befunge93.Load(program);

        using var swStdout = new StringWriter();
        Console.SetOut(swStdout);
        using var swStdIn = new StringReader("65 ");
        Console.SetIn(swStdIn);

        _befunge93.Run(TimeSpan.FromSeconds(1));
        Assert.That(swStdout.ToString(), Is.EqualTo("A"));

        TestContext.Out.WriteLine($"Result: '{swStdout}'");
    }

    [Test]
    public void NegativeNumericInput()
    {
        var program = new[] { "&@" };
        _befunge93.Load(program);

        using var swStdIn = new StringReader("-1 ");
        Console.SetIn(swStdIn);

        _befunge93.Run(TimeSpan.FromSeconds(1));

        var expected = new Stack<int>(new[] { -1 });
        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expected));
        actual.PrintToTest();
    }

    [Test]
    public void HelloWorld()
    {
        //
        // CC0 -- Chris Pressey -- 9/5/93
        // https://github.com/catseye/Befunge-93/blob/master/eg/hello.bf
        //
        var program = new[]
        {
            "                 v", ">v\"Hello world!\"0<", ",:                ", "^_25*,@           "
        };
        _befunge93.Load(program);

        using var sw = new StringWriter();
        Console.SetOut(sw);

        _befunge93.Run(TimeSpan.FromSeconds(1));

        Assert.That(sw.ToString(), Is.EqualTo("Hello world!\n"));
        TestContext.Out.WriteLine($"Result: '{sw}'");
    }
}
