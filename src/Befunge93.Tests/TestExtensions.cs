using NUnit.Framework;

namespace Befunge93.Tests;

public static class TestExtensions
{
    public static void PrintToTest<T>(this IEnumerable<T> data)
    {
        TestContext.Out.WriteLine($"---- {data.GetType()} ----");
        foreach(T value in data)
        {
            TestContext.Out.WriteLine(value);
        }
    }
}
