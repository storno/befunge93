using NUnit.Framework;

namespace Befunge93.Tests;

public class LoadingTests
{
    private Befunge93 _befunge93;

    [OneTimeSetUp]
    public void OneTimeSetup()
    {
        _befunge93 = new Befunge93();
    }

    [Test]
    public void HorizontalTruncation()
    {
        //
        // CC0 -- Chris Pressey -- 12/1/96
        // https://github.com/catseye/Befunge-93/blob/master/eg/selflis2.bf
        //
        var program = new[]
        {
            "\">:#,_66*2-,@This prints itself out backwards......  but it has to be 80x1 cells^^^^^",
        };
        _befunge93.Load(program);

        using var sw = new StringWriter();
        Console.SetOut(sw);

        _befunge93.Run(TimeSpan.FromSeconds(1));

        Assert.That(sw.ToString(), Is.EqualTo("sllec 1x08 eb ot sah ti tub  ......sdrawkcab tuo flesti stnirp sihT@,-2*66_,#:>\""));
        TestContext.Out.WriteLine($"Result: '{sw}'");
    }
    
    [Test]
    public void VerticalTruncation()
    {
        //
        // CC0 -- Chris Pressey -- 12/1/96
        // https://github.com/catseye/Befunge-93/blob/master/eg/selflis2.bf
        //
        var program = new[]
        {
            "0  v@",
            "1   1",
            "2    ",
            "3    ",
            "4    ",
            "5    ",
            "6    ",
            "7    ",
            "8    ",
            "9    ",
            "10   ",
            "11   ",
            "12   ",
            "13   ",
            "14   ",
            "15   ",
            "16   ",
            "17   ",
            "18   ",
            "19   ",
            "20   ",
            "21   ",
            "22   ",
            "23   ",
            "24 @ ",
            "25 >^"  // this should not make it into the program
        };
        _befunge93.Load(program);
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var expected = new Stack<int>(new[] { 0 });
        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expected));
        actual.PrintToTest();
    }

    [Test]
    public void LoadFile()
    {
        var filename = $"{Guid.NewGuid()}.b93";
        File.WriteAllText(filename, ">123@");

        _befunge93.Load(filename);
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var expected = new Stack<int>(new[] { 1, 2, 3 });
        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expected));
        actual.PrintToTest();

        File.Delete(filename);
    }
    
    [Test]
    public void InvalidLoadFromPlayfield()
    {
        var program = new char[Befunge93.Width * Befunge93.Height + 1];

        Assert.Throws<InvalidOperationException>(
            () => _befunge93.Load(program));
    }

    [Test]
    public void LoadFromPlayfield()
    {
        var program = new char[Befunge93.Width * Befunge93.Height];
        Array.Fill(program, ' ');

        // this program just runs the IP around the edges of the torus
        program[0] = 'v';
        program[1] = '@';
        program[2] = '4';
        program[Befunge93.Width - 2] = '3';
        program[Befunge93.Width - 1] = '<';
        program[Befunge93.Width * (Befunge93.Height - 2)] = '1';
        program[Befunge93.Width * (Befunge93.Height - 1)] = '>';
        program[Befunge93.Width * Befunge93.Height - 2] = '2';
        program[Befunge93.Width * Befunge93.Height - 1] = '^';

        _befunge93.Load(program);
        _befunge93.Run(TimeSpan.FromSeconds(1));

        var expected = new Stack<int>(new[] { 1, 2, 3, 4 });
        var actual = _befunge93.GetStack();

        Assert.That(actual, Is.EqualTo(expected));
        actual.PrintToTest();
    }
}
