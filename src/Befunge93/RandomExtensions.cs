namespace Befunge93;

// https://stackoverflow.com/a/60847339
public static class RandomExtensions
{   
    public static T NextEnum<T>(this Random random)
    {
        var values = Enum.GetValues(typeof(T));
        // Unsure if null suppression is the correct move here.
        return (T)values.GetValue(random.Next(values.Length))!;
    }
}
