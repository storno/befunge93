using System.Diagnostics;

namespace Befunge93;

public enum Direction
{
    Right = 0,
    Down,
    Left,
    Up,
}

public class ProcessorHaltedException : Exception
{
};

public class InvalidInstructionException : Exception
{
    public InvalidInstructionException(int value)
        : base(value is >= 32 and <= 127
            ? ((char)value).ToString()
            : value.ToString("x8").ToLowerInvariant())
    {
    }
};

public class Befunge93
{
    public const int Width = 80;
    public const int Height = 25;

    private readonly Random _rng = new();
    private readonly Stopwatch _executionTimer = new();

    private TimeSpan _maximumExecutionTime;
    private int[] _torus = Array.Empty<int>();
    private Direction _programCounter;
    private (int x, int y) _instructionPointer;
    private Stack<int> _stack = new();
    private bool _stringMode;

    public Befunge93()
    {
        Reset();
    }

    public Stack<int> GetStack() => new Stack<int>(_stack.Reverse());

    public void Load(string filename)
    {
        Load(File.ReadAllLines(filename));
    }

    public void Load(IEnumerable<string> playfield)
    {
        Reset();

        var lineNum = 0;
        foreach (var line in playfield)
        {
            if (line.Length > Width)
            {
                Console.Error.WriteLine($"Warning: program line too long, truncated to {Width} characters.");
            }

            if (lineNum >= Height)
            {
                Console.Error.WriteLine($"Warning: program longer than {Height} lines. Ignoring remaining lines.");
                break;
            }

            for (var n = 0; n < line.Length; n++)
            {
                SetData(n, lineNum, line[n]);
            }

            lineNum++;
        }
    }

    public void Load(char[] playfield)
    {
        Reset();

        if (playfield.Length != Width * Height)
        {
            throw new InvalidOperationException("Geometry invalid.");
        }

        Array.Copy(playfield, _torus, Width * Height);
    }

    public void Run(TimeSpan maximumExecutionTime)
    {
        _maximumExecutionTime = maximumExecutionTime;
        Run();
    }

    public void Run()
    {
        if (_maximumExecutionTime != TimeSpan.Zero)
        {
            _executionTimer.Restart();
        }

        while (true)
        {
            if (_maximumExecutionTime != TimeSpan.Zero &&
                _executionTimer.ElapsedMilliseconds > _maximumExecutionTime.TotalMilliseconds)
            {
                _executionTimer.Stop();
                throw new TimeoutException("Execution time exceeded.");
            }

            try
            {
                ExecuteInstruction();
            }
            catch (ProcessorHaltedException)
            {
                break;
            }

            MoveInstructionPointer();
        }
    }

    private void Reset()
    {
        _torus = new int[Width * Height];
        Array.Fill(_torus, ' ');

        _maximumExecutionTime = TimeSpan.Zero;
        _executionTimer.Reset();
        _programCounter = Direction.Right;
        _instructionPointer = (0, 0);
        _stack = new Stack<int>();
        _stringMode = false;
    }

    private void MoveInstructionPointer()
    {
        // Perform movement
        switch (_programCounter)
        {
            case Direction.Up:
                _instructionPointer.y -= 1;
                break;

            case Direction.Down:
                _instructionPointer.y += 1;
                break;

            case Direction.Left:
                _instructionPointer.x -= 1;
                break;

            case Direction.Right:
                _instructionPointer.x += 1;
                break;
        }

        // Wrap
        if (_instructionPointer.y < 0)
        {
            _instructionPointer.y = Height - 1;
        }

        if (_instructionPointer.y >= Height)
        {
            _instructionPointer.y = 0;
        }

        if (_instructionPointer.x < 0)
        {
            _instructionPointer.x = Width - 1;
        }

        if (_instructionPointer.x >= Width)
        {
            _instructionPointer.x = 0;
        }
    }

    private void ExecuteInstruction()
    {
        var currentData = ReadData(_instructionPointer.x, _instructionPointer.y);

        // Are we in stringmode?
        if (_stringMode)
        {
            // Do we need to come out of stringmode?
            if (currentData == '"')
            {
                _stringMode = false;
                return;
            }

            // Push whatever we got to the stack, we're done.
            _stack.Push(currentData);
            return;
        }

        switch (currentData)
        {
            // Basic Mathematics
            case '+':
                _stack.TryPop(out var aR1);
                _stack.TryPop(out var aR2);
                _stack.Push(aR2 + aR1);
                break;

            case '-':
                _stack.TryPop(out var bR1);
                _stack.TryPop(out var bR2);
                _stack.Push(bR2 - bR1);
                break;

            case '*':
                _stack.TryPop(out var cR1);
                _stack.TryPop(out var cR2);
                _stack.Push(cR2 * cR1);
                break;

            case '/':
                _stack.TryPop(out var dR1);
                _stack.TryPop(out var dR2);
                _stack.Push(dR1 == 0 ? 0 : dR2 / dR1);
                break;

            // Modulo has UB in the spec (what to do if negative?)
            case '%':
                _stack.TryPop(out var eR1);
                _stack.TryPop(out var eR2);
                _stack.Push(eR2 % eR1);
                break;

            case '!': // not
                _stack.TryPop(out var fR1);
                _stack.Push(fR1 == 0 ? 1 : 0);
                break;

            case '`': // greater than
                _stack.TryPop(out var gR1);
                _stack.TryPop(out var gR2);
                _stack.Push(gR2 > gR1 ? 1 : 0);
                break;

            // Set PC direction
            case '>':
                _programCounter = Direction.Right;
                break;

            case '<':
                _programCounter = Direction.Left;
                break;

            case '^':
                _programCounter = Direction.Up;
                break;

            case 'v':
                _programCounter = Direction.Down;
                break;

            case '?':
                _programCounter = _rng.NextEnum<Direction>();
                break;

            // Branches
            case '_': // Horizontal If
                _stack.TryPop(out var hR1);
                _programCounter = hR1 == 0 ? Direction.Right : Direction.Left;
                break;

            case '|': // Vertical If
                _stack.TryPop(out var iR1);
                _programCounter = iR1 == 0 ? Direction.Down : Direction.Up;
                break;

            case '"':
                _stringMode = true;
                break;

            // Stack Manipulation
            case ':':
                _stack.TryPop(out var jR1);
                _stack.Push(jR1);
                _stack.Push(jR1);
                break;

            case '\\':
                _stack.TryPop(out var kR1);
                _stack.TryPop(out var kR2);
                _stack.Push(kR1);
                _stack.Push(kR2);
                break;

            case '$':
                _stack.TryPop(out var _);
                break;

            // Input
            case '&':
                var charsTyped = new List<char>();
                var number1 = 0;
                while (true)
                {
                    var nextChar = (char)Console.Read();
                    if (nextChar is >= '0' and <= '9' or '-')
                    {
                        charsTyped.Add(nextChar);
                        _ = int.TryParse(charsTyped.ToArray(), out number1);
                    }
                    else
                    {
                        break;
                    }
                }

                _stack.Push(number1);
                break;

            case '~':
                var letter1 = Console.Read();
                if (letter1 > 127)
                {
                    throw new InvalidOperationException("Non-ASCII character input.");
                }

                _stack.Push(letter1);
                break;

            // Output
            case '.':
                _stack.TryPop(out var number2);
                Console.Write($"{number2} ");
                break;

            case ',':
                _stack.TryPop(out var letter2);
                if (letter2 > 127)
                {
                    throw new InvalidOperationException("Non-ASCII character output.");
                }

                Console.Write((char)letter2);
                break;

            // Program Counter Manipulation
            case '#': // bridge
                MoveInstructionPointer();
                break;

            // Self-Modification
            case 'g':
                _stack.TryPop(out var lR1);
                _stack.TryPop(out var lR2);
                _stack.Push(ReadData(x: lR2, y: lR1));
                break;

            case 'p':
                _stack.TryPop(out var mR1); // y
                _stack.TryPop(out var mR2); // x
                _stack.TryPop(out var mR3); // data
                SetData(x: mR2, y: mR1, d: mR3);
                break;

            case '@':
                throw new ProcessorHaltedException();

            case ' ':
                break;

            case '0':
            case '1':
            case '2':
            case '3':
            case '4':
            case '5':
            case '6':
            case '7':
            case '8':
            case '9':
                _stack.Push(currentData - '0');
                break;

            default:
                throw new InvalidInstructionException(currentData);
        }
    }

    private int ReadData(int x, int y) => _torus[(Width * y) + x];
    private void SetData(int x, int y, int d) => _torus[(Width * y) + x] = d;
}
