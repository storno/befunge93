using System.CommandLine;
using System.Diagnostics;

namespace Befunge93;

internal static class Program
{
    public static async Task Main(string[] args)
    {
        var rootCommand = new RootCommand();

        var configOption = new Option<string?>(
            new[] { "-t", "--timeout" },
            description: "Maximum execution time in seconds.",
            getDefaultValue: () => null
        );
        rootCommand.Add(configOption);

        var urlArgument = new Argument<string>(
            "filename",
            description: "Filename to load."
        );
        rootCommand.Add(urlArgument);
        rootCommand.SetHandler(async (timeout, filename) => { await ExecuteFile(filename, int.Parse(timeout ?? "0")); }, configOption, urlArgument);

        await rootCommand.InvokeAsync(args);
    }

    private static async Task ExecuteFile(string filename, int timeout)
    {
        var bf93 = new Befunge93();
        var sw = new Stopwatch();

        try
        {
            bf93.Load(filename);
            sw.Start();

            if (timeout == 0)
            {
                await Console.Error.WriteLineAsync($"Beginning execution of {filename}, no timeout specified.");
                bf93.Run();
            }
            else 
            {
                await Console.Error.WriteLineAsync($"Beginning execution of {filename}, maximum execution time: {timeout} seconds.");
                bf93.Run(TimeSpan.FromSeconds(timeout));
            }
        }
        catch (ProcessorHaltedException)
        {
            await Console.Error.WriteLineAsync($"Program executed normally in {sw.Elapsed.TotalSeconds} seconds.");
        }
        catch (TimeoutException)
        {
            await Console.Error.WriteLineAsync("Program execution killed due to timeout.");
        }

        await Task.CompletedTask;
    }
}
